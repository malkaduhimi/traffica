<?php
/***
 * Here comes the definition of environments and adding pages to them
 * 
 * For example:
 * $public = $traffica->make('public', 'public', true); // make public environment
 * $public->addPage(new Page, true); // add a page object to the environment
*/