<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once(__DIR__ . '/vendor/autoload.php');

traffica()->init();

include('start/load_pages.php');

traffica()->execute();
echo traffica()->getView();